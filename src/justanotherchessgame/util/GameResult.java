package justanotherchessgame.util;

/**
 *Enum class used to represent the game result.
 */
public enum GameResult {

    /**
     * First one represent the white win, second one represent the black win, third one represent the stale.
     */
    WHITE_WIN, BLACK_WIN, STALEMATE
}
