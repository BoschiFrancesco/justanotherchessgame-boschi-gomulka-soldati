package justanotherchessgame.model;

import java.util.List;

import justanotherchessgame.controller.Controller;
import justanotherchessgame.util.GameResult;

/**
 * Class representing a Real Player.
 */
public class LocalPlayer extends Player {

    private Controller controller;

    /**
     * Constructor of the local player.
     * @param color is a boolean representing player pieces color.
     */
    public LocalPlayer(final boolean color) {
        super(color);
    }

    /**
     * Function used to check is the controller is set.
     * @return true if the controller is set, false otherwise.
     */
    private boolean controlsBoard() {
        return controller != null;
    }

    /**
     * Setter of the Controller relative to current player.
     * @param c is the controller which will be applied to the current player
     */
    public void setController(final Controller c) {
        System.out.println("set controller player");
        controller = c;
    }

    @Override
    public final void notifyMove(final MoveInfo m) {
        if (controlsBoard()) {
            System.out.println("notifying move to board controller");
            controller.notifyMove(m);
        }
    }

    /**
     * Function used to get the list of moves from the beginning until a given index.
     * @param index the limit of the history.
     * @return the list of moves.
     */
    public final List<MoveInfo> getList(final int index) {
        return getMoveHistory().subList(0, index);
    }

    @Override
    public final void notifyResult(final GameResult result) {
        if (controller != null) {
            controller.notifyGameEnd(result);
        }
    }

    @Override
    public final void notifyStart() {
        final List<MoveInfo> moves = this.getGame().getMoveHistory();
        moves.forEach(m -> notifyMove(m));
    }
}
