package justanotherchessgame.model;

import justanotherchessgame.util.GameResult;

/**
 * Function used to represent the blitz game, with time.
 */
public class BlitzGame extends ClassicGame {
    // Time left (in milliseconds)
    private int whiteTime;
    private int blackTime;
    // Time gain (in milliseconds)
    private final int extraTime;
    // Last time
    private long last = System.currentTimeMillis();
    // Future to handle time over
    private Thread timeOver;

    /**
     * Class constructor.
     * @param white is the white player.
     * @param black is the black player.
     * @param startTime is the starting time.
     * @param moveTime is the time increment after each move
     */
    public BlitzGame(final Player white, final Player black, final int startTime, final int moveTime) {
        super(white, black);
        // Set time fields
        whiteTime = startTime * 1000;
        blackTime = startTime * 1000;
        extraTime = moveTime * 1000;
    }

    /**
     * Function used to wait an event.
     * @param color is the player color.
     */
    private void wait(final boolean color) {
        System.out.println("Thread started.");
        try {
            System.out.println("Wait for" + (color ? whiteTime : blackTime));
            Thread.sleep(color ? whiteTime : blackTime);
        } catch (Exception ex) {
            // Do nothing
            System.out.println("waiting thread stopped");
            return;
        }
        // Thread was not stopped
        endGame(color ? GameResult.BLACK_WIN : GameResult.WHITE_WIN);
    }

    @Override
    protected final void applyMove(final MoveInfo move) {
        updateTimer(nextColor());
        super.applyMove(move);
    }
    private void updateTimer(final boolean color) {
        System.out.println("Update timer");
        // Decrease time to currentPlayer
        final long delta = System.currentTimeMillis() - last;
        if (color) {
            whiteTime -= delta;
            whiteTime += extraTime;
        } else {
            blackTime -= delta;
            blackTime += extraTime;
        }
        // Stop thread
        timeOver.interrupt();
        // Set last
        last = System.currentTimeMillis();
        // Run thread
        startWait(!color);
    }

    /**
     * Starts a thread waiting for a given player's time to run out.
     * @param color: The player we are waiting for.
     */
    private void startWait(final boolean color) {
        timeOver = new Thread(() -> wait(!color));
        timeOver.start();
    }

    @Override
    public final void start() {
        super.start();
        // Also starts timer
        startWait(nextColor());
    }

    @Override
    public final void stopGame() {
        timeOver.interrupt();
        super.stopGame();
    }
}
